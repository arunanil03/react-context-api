import React,{useState,createContext} from 'react'

export const ProvideTheme=createContext();

export const ThemeProvider=(props)=>{
const[theme,setTheme]=useState("green")

return(
<ProvideTheme.Provider value={[theme,setTheme]}>
 {props.children}
</ProvideTheme.Provider>
)
}