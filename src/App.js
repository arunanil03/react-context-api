import React from 'react';
import './App.css';
import {ThemeProvider} from "./context_provider";
import {Circle,Square,ThemePicker} from "./shapes"
 
function App() {
  return (
    
    <ThemeProvider> 
    <div className="App">
     <Square/>
     <Circle/>
     </div>
     <div className="App">
     <ThemePicker/>
     </div>
    </ThemeProvider>
 
  );
}


export default App;
